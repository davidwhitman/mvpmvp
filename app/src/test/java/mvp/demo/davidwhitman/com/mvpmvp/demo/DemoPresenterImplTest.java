package mvp.demo.davidwhitman.com.mvpmvp.demo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

/**
 * @author David Whitman on Jun 20, 2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class DemoPresenterImplTest {
    DemoPresenter presenter;
    @Mock DemoView view;

    @Before
    public void setUp() throws Exception {
        presenter = new DemoPresenterImpl();
        presenter.bindView(view);
    }

    @Test
    public void test_onClickedClickyButton_showsSuccessMessage() throws Exception {
        presenter.onClickedClickyButton();

        verify(view).displaySuccessMessage();
    }

    @Test
    public void test_onClickedClickyButton_changesButtonColor() throws Exception {
        presenter.onClickedClickyButton();

        verify(view).setRandomButtonColor();
    }
}