package mvp.demo.davidwhitman.com.mvpmvp.dagger;

import javax.inject.Singleton;

import dagger.Component;
import mvp.demo.davidwhitman.com.mvpmvp.demo.DemoPresenter;

/**
 * @author David Whitman on Jun 20, 2016.
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    DemoPresenter getDemoPresenter();
}
