package mvp.demo.davidwhitman.com.mvpmvp.util;



/**
 * Represents a way to get an object only when needed.
 *
 * @author David Whitman on May 19, 2016.
 */
public interface Task<T> {
    T get();
}