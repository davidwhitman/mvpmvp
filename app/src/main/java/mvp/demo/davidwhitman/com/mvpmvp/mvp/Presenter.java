package mvp.demo.davidwhitman.com.mvpmvp.mvp;

/**
 * @author David Whitman
 *         Date: 2/29/2016
 */
public interface Presenter<V extends MvpView> {
    void bindView(V view);

    void unbindView();
}
