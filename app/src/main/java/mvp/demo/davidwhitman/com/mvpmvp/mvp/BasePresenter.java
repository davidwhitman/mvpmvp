package mvp.demo.davidwhitman.com.mvpmvp.mvp;

import android.support.annotation.NonNull;

import java.lang.ref.WeakReference;

/**
 * @author David Whitman
 *         Date: 2/23/2016
 */
public abstract class BasePresenter<V extends MvpView> implements Presenter<V> {
    private WeakReference<V> view;

    public void bindView(@NonNull V view) {
        this.view = new WeakReference<>(view);
    }

    public void unbindView() {
        this.view = null;
    }

    protected final V view() {
        if (view == null) {
            return null;
        } else {
            return view.get();
        }
    }

    protected final boolean hasView() {
        return view() != null;
    }
}
