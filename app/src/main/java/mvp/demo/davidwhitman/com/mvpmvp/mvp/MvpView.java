package mvp.demo.davidwhitman.com.mvpmvp.mvp;

/**
 * A view, used in the MVP architecture.
 *
 * @author David Whitman
 *         Date: 2/26/2016
 */
public interface MvpView {
}
