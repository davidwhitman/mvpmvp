package mvp.demo.davidwhitman.com.nomvp;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import java.util.Random;

import mvp.demo.davidwhitman.com.mvpmvp.R;

public class DemoActivity extends AppCompatActivity {
    View button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.super_clicky_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displaySuccessMessage();
                setRandomButtonColor();
            }
        });
    }

    private void displaySuccessMessage() {
        Toast.makeText(DemoActivity.this, "I've been clicked!", Toast.LENGTH_SHORT).show();
    }

    private void setRandomButtonColor() {
        Random rnd = new Random();
        @ColorInt int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        button.setBackgroundColor(color);
    }
}
