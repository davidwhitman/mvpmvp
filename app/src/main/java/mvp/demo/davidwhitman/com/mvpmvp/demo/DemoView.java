package mvp.demo.davidwhitman.com.mvpmvp.demo;

import mvp.demo.davidwhitman.com.mvpmvp.mvp.MvpView;

/**
 * @author David Whitman on Jun 20, 2016.
 */
public interface DemoView extends MvpView{
    void displaySuccessMessage();

    void setRandomButtonColor();
}
