package mvp.demo.davidwhitman.com.mvpmvp.mvp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import mvp.demo.davidwhitman.com.mvpmvp.util.Task;

/**
 * Subclasses {@link Fragment}. Handles all logic for creating and destroying a {@link PresenterLoader},
 * which creates and maintains a {@link Presenter} for use by a {@link MvpView}
 * <p/>
 * NOTE: In the future, this could be rolled into BaseFragment instead of being a subclass.
 * We don't want too many subclasses like this.
 *
 * @author David Whitman
 *         Date: 3/24/2016
 * @see <a href="https://medium.com/@czyrux/presenter-surviving-orientation-changes-with-loaders-6da6d86ffbbf">Surviving Orientation Changes with Loaders</a>
 */
public abstract class MvpFragment<T extends Presenter> extends Fragment
        implements LoaderManager.LoaderCallbacks<T>, MvpView {

    /**
     * For Fragments, the presenter will be non-null (loaded) in onResume.
     * For Activities, the presenter will be non-null (loaded) in onStart.
     */
    protected T presenter;
    protected final static int LOADER_ID = (int) (Math.random() * Integer.MAX_VALUE);

    @Override
    public void onStart() {
        super.onStart();

        getActivity().getSupportLoaderManager().initLoader(LOADER_ID, null, this);

        // If onLoadFinished hasn't been called yet, the presenter will be null
        if (presenter != null) {
            //noinspection unchecked
            presenter.bindView(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.unbindView();
    }

    @Override
    public Loader<T> onCreateLoader(int id, Bundle args) {
        if (id == LOADER_ID) {
            return new PresenterLoader<>(getActivity(), new Task<T>() {
                @Override
                public T get() {
                    return createNewPresenter();
                }
            });
        } else {
            return null;
        }
    }

    protected abstract T createNewPresenter();

    /**
     * For Fragments, the presenter will be non-null (loaded) in onResume.
     * For Activities, the presenter will be non-null (loaded) in onStart.
     */
    @Override
    public void onLoadFinished(Loader<T> loader, T data) {
        this.presenter = data;
        //noinspection unchecked
        presenter.bindView(this);
    }

    @Override
    public void onLoaderReset(Loader<T> loader) {
        presenter = null;
    }
}
