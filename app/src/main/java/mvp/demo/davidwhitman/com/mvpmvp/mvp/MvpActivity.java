package mvp.demo.davidwhitman.com.mvpmvp.mvp;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;

import mvp.demo.davidwhitman.com.mvpmvp.util.Task;

/**
 * @author David Whitman on Jun 20, 2016.
 */
public abstract class MvpActivity<P extends Presenter> extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<P>, MvpView {

    /**
     * For Fragments, the presenter will be non-null (loaded) in onResume.
     * For Activities, the presenter will be non-null (loaded) in onStart.
     */
    protected P presenter;
    protected final static int LOADER_ID = (int) (Math.random() * Integer.MAX_VALUE);

    @Override
    public void onStart() {
        super.onStart();

        getSupportLoaderManager().initLoader(LOADER_ID, null, this);

        // If onLoadFinished hasn't been called yet, the presenter will be null
        if (presenter != null) {
            //noinspection unchecked
            presenter.bindView(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.unbindView();
    }

    @Override
    public Loader<P> onCreateLoader(int id, Bundle args) {
        if (id == LOADER_ID) {
            return new PresenterLoader<>(this, new Task<P>() {
                @Override
                public P get() {
                    return createNewPresenter();
                }
            });
        } else {
            return null;
        }
    }

    protected abstract P createNewPresenter();

    /**
     * For Fragments, the presenter will be non-null (loaded) in onResume.
     * For Activities, the presenter will be non-null (loaded) in onStart.
     */
    @Override
    public void onLoadFinished(Loader<P> loader, P data) {
        this.presenter = data;
        //noinspection unchecked
        presenter.bindView(this);
    }

    @Override
    public void onLoaderReset(Loader<P> loader) {
        presenter = null;
    }
}
