package mvp.demo.davidwhitman.com.mvpmvp.dagger;

import dagger.Module;
import dagger.Provides;
import mvp.demo.davidwhitman.com.mvpmvp.demo.DemoPresenter;
import mvp.demo.davidwhitman.com.mvpmvp.demo.DemoPresenterImpl;

/**
 * @author David Whitman on Jun 20, 2016.
 */
@Module
public class AppModule {
    @Provides
    public DemoPresenter provideDemoPresenter(DemoPresenterImpl demoPresenter) {
        return demoPresenter;
    }
}
