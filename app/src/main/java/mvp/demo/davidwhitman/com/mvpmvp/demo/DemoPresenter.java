package mvp.demo.davidwhitman.com.mvpmvp.demo;

import mvp.demo.davidwhitman.com.mvpmvp.mvp.Presenter;

/**
 * @author David Whitman on Jun 20, 2016.
 */
public interface DemoPresenter extends Presenter<DemoView> {
    void onClickedClickyButton();
}
