package mvp.demo.davidwhitman.com.mvpmvp.demo;

import javax.inject.Inject;

import mvp.demo.davidwhitman.com.mvpmvp.mvp.BasePresenter;

/**
 * @author David Whitman on Jun 20, 2016.
 */
public class DemoPresenterImpl extends BasePresenter<DemoView> implements DemoPresenter {
    @Inject
    public DemoPresenterImpl() {
    }

    @Override
    public void onClickedClickyButton() {
        view().displaySuccessMessage();
        view().setRandomButtonColor();
    }
}
