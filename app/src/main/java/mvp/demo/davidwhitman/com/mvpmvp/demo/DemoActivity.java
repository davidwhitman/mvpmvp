package mvp.demo.davidwhitman.com.mvpmvp.demo;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.view.View;
import android.widget.Toast;

import java.util.Random;

import mvp.demo.davidwhitman.com.mvpmvp.App;
import mvp.demo.davidwhitman.com.mvpmvp.R;
import mvp.demo.davidwhitman.com.mvpmvp.mvp.MvpActivity;

public class DemoActivity extends MvpActivity<DemoPresenter> implements DemoView {
    View button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.super_clicky_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickedClickyButton();
            }
        });
    }

    @Override
    protected DemoPresenter createNewPresenter() {
        return App.getAppComponent().getDemoPresenter();
    }

    @Override
    public void displaySuccessMessage() {
        Toast.makeText(DemoActivity.this, "I've been clicked!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setRandomButtonColor() {
        Random rnd = new Random();
        @ColorInt int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        button.setBackgroundColor(color);
    }

    private void onClickedClickyButton() {
        presenter.onClickedClickyButton();
    }
}
