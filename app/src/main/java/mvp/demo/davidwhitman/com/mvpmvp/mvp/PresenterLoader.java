package mvp.demo.davidwhitman.com.mvpmvp.mvp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;


import javax.inject.Inject;

import mvp.demo.davidwhitman.com.mvpmvp.util.Task;

/**
 * Creates and keeps a {@link Presenter} across configuration changes. Lifecycle is managed by the system.
 *
 * @author David Whitman
 *         Date: 3/24/2016
 */
public class PresenterLoader<T extends Presenter> extends Loader<T> {
    /**
     * New presenter, provided by implementing class. We use this if we're not loading a saved presenter.
     */
    @NonNull
    protected Task<T> getNewPresenterTask;

    /**
     * The presenter that is maintained across config change and used by the view.
     * Instantiated from <code>getNewPresenterTask</code>
     */
    @Nullable private T presenter;

    /**
     * Stores away the application context associated with context.
     * Since Loaders can be used across multiple activities it's dangerous to
     * store the context directly; always use {@link #getContext()} to retrieve
     * the Loader's Context, don't use the constructor argument directly.
     * The Context returned by {@link #getContext} is safe to use across
     * Activity instances.
     *
     * @param context             used to retrieve the application context.
     * @param getNewPresenterTask a task to create a new instance of the presenter that will be used
     *                            when not restoring from a config change.
     */
    @Inject
    public PresenterLoader(@NonNull Context context, @NonNull Task<T> getNewPresenterTask) {
        super(context);
        this.getNewPresenterTask = getNewPresenterTask;
    }

    /**
     * Subclasses must implement this to take care of loading their data,
     * as per {@link #startLoading()}.  This is not called by clients directly,
     * but as a result of a call to {@link #startLoading()}.
     */
    @Override
    protected void onStartLoading() {
        // If the presenter is non-null, we're coming from a config change and we re-use it
        if (presenter != null) {
            deliverResult(presenter);
        } else { // Otherwise, we get a new presenter
            forceLoad();
        }
    }

    /**
     * Subclasses must implement this to take care of requests to {@link #forceLoad()}.
     * This will always be called from the process's main thread.
     */
    @Override
    protected void onForceLoad() {
        presenter = getNewPresenterTask.get();
        deliverResult(presenter);
    }

    /**
     * Subclasses must implement this to take care of resetting their loader,
     * as per {@link #reset()}.  This is not called by clients directly,
     * but as a result of a call to {@link #reset()}.
     * This will always be called from the process's main thread.
     */
    @Override
    protected void onReset() {
        presenter = null;
    }
}
