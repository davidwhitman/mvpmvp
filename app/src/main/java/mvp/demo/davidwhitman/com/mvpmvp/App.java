package mvp.demo.davidwhitman.com.mvpmvp;

import android.app.Application;

import mvp.demo.davidwhitman.com.mvpmvp.dagger.AppComponent;
import mvp.demo.davidwhitman.com.mvpmvp.dagger.AppModule;
import mvp.demo.davidwhitman.com.mvpmvp.dagger.DaggerAppComponent;

/**
 * @author David Whitman on Jun 20, 2016.
 */
public class App extends Application {
    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule())
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
